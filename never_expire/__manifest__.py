{
    "name": "Never Expire",
    "summary": """This Module set Database Never Expire""",
    "maintainer": "sayhi2awais@gmail.com",
    "category": "Tools",
    "author": "Muhammad Awais",
    "license": "LGPL-3",
    "version": "17.0",
    "website": "https://bit.ly/3E0SdcX",
    "depends": ["mail"],
    "data": ["views/update.xml"],
    "sequence": 999,
}
