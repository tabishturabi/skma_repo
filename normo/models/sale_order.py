from odoo import models, fields, api
import requests
import json
import logging

_logger = logging.getLogger(__name__)


class SelectionOption(models.Model):
    _name = 'selection.option'
    _description = 'Selection Option'

    name = fields.Char('Name', required=True)
    type = fields.Selection([
        ('pago', 'Condiciones de pago'),
        ('entrega', 'Condiciones de entrega'),
        ('tiempo_entrega', 'Tiempo de entrega'),
        ('tiempo_instalacion', 'Tiempo de Instalación/Armado')
    ], required=True)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    payment_conditions = fields.Many2many(
        'selection.option',
        'sale_order_payment_conditions_rel',
        'sale_order_id', 'selection_option_id',
        string='Condiciones de pago',
        domain=[('type', '=', 'pago')]
    )
    delivery_conditions = fields.Many2many(
        'selection.option',
        'sale_order_delivery_conditions_rel',
        'sale_order_id', 'selection_option_id',
        string='Condiciones de entrega',
        domain=[('type', '=', 'entrega')]
    )
    delivery_times = fields.Many2many(
        'selection.option',
        'sale_order_delivery_times_rel',
        'sale_order_id', 'selection_option_id',
        string='Tiempo de entrega',
        domain=[('type', '=', 'tiempo_entrega')]
    )
    installation_times = fields.Many2many(
        'selection.option',
        'sale_order_installation_times_rel',
        'sale_order_id', 'selection_option_id',
        string='Tiempo de Instalación/Armado',
        domain=[('type', '=', 'tiempo_instalacion')]
    )